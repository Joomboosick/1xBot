﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1xBot.Models
{
    public class FilterModel
    {
        [PrimaryKey, AutoIncrement]
        public int? id { get; set; }
        public bool Enable { get; set; }
        public string Ligue { get; set; }
        public string Time { get; set; }
        public string ScoreDiff { get; set; }
        public string Penalty { get; set; }
        public string Line { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Coef { get; set; }
        public string Betsize { get; set; }
    }
}
