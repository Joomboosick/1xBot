﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1xBot.Models
{
    public class Settings
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Balance { get; set; }
        public string Key { get; set; }
        public bool FixBetSizeEnable { get; set; }
        public string FixBetSize { get; set; }
    }
}
