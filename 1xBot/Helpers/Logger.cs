﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1xBot.Helpers
{
    public class Logger
    {
        readonly private string filename;
        public Logger()
        {
            if (!Directory.Exists(@"logs"))
            {
                Directory.CreateDirectory(@"logs");
            }
            filename = @"logs/"+ DateTime.Now.ToString().Replace(':','-') + @"_1xbot.txt";
            File.CreateText(filename);
        }
        public void Warn(string exmessage, string pos)
        {
            File.AppendAllText(filename, "[WARN][" + DateTime.Now.ToString() + "]__" + pos + "____" + exmessage + "\n");
        }
        public void Info(string info, string pos)
        {
            File.AppendAllText(filename, "[INFO][" + DateTime.Now.ToString() + "]__" + pos + "____" + info+"\n");
        }
        public void Error(string exmessage, string pos)
        {
            File.AppendAllText(filename, "[ERROR][" + DateTime.Now.ToString() + "]__" + pos + "____" + exmessage + "\n");
        }

    }
}
