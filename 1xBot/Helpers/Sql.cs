﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _1xBot.Helpers
{
    public class Sql
    {
        readonly SQLiteConnection filtersDb;
        readonly SQLiteConnection betsDb;
        public Sql(string filtersPath, string betsPath)
        {
            if (!File.Exists(filtersPath))
                File.Create(filtersPath);
            
            if (!File.Exists(betsPath))
                File.Create(betsPath);


            filtersDb = new SQLiteConnection(filtersPath);
            betsDb = new SQLiteConnection(betsPath);
            filtersDb.CreateTable<Models.FilterModel>();
            betsDb.CreateTable<Models.BetModel>();
        }
        public int AddFilter(Models.FilterModel filter)
        {
            return filtersDb.Insert(filter);
        }
        public int UpdateFilter(Models.FilterModel filter)
        {
            return filtersDb.Update(filter);
        }
        public int UpdateBet(Models.BetModel bet)
        {
            return betsDb.Update(bet);
        }
        public int DeleteFilter(Models.FilterModel filter)
        {
            return filtersDb.Delete(filter);
        }
        public int DeleteBet(Models.BetModel bet)
        {
            return betsDb.Delete(bet);
        }
        public int DeleteAllBets()
        {
            return betsDb.DeleteAll<Models.BetModel>();
        }

        public int AddBet(Models.BetModel Bet)
        {
            return betsDb.Insert(Bet);
        }
        public List<Models.FilterModel> GetFilters()
        {
            return filtersDb.Table<Models.FilterModel>().ToList();
        }
        public List<Models.BetModel> GetBets()
        {
            return betsDb.Table<Models.BetModel>().ToList();
        }
    }
}
