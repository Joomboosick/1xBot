﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using _1xBot.Models;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using Excel = Microsoft.Office.Interop.Excel;

namespace _1xBot
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public BetModel CurrentBetInForm { get; set; }
        public FilterModel CurrentFilterInForm { get; set; }
        public Encoding encode = System.Text.Encoding.GetEncoding("utf-8");
        public ObservableCollection<BetModel> Bets {get;set;}
        public ObservableCollection<FilterModel> Filters {get;set;}
       
        private string CurentLigue;
        string CurrentLineXpath;
        private int CurentEventMinuts;
        private int CurentEventSeconds;
        private string CurentEventName;
        private string CurrentEventCommandsNames;
        private int CurentEventDeletes;
        int bank;
        private float CurentEventCf;
        private string CurentEventLine;
        private int CurentEventScoreDiff;
        IWebDriver webDriver;
        readonly string MainUrl = "https://1xstavka.ru/";
        IDictionary<string,DateTime> IgnoreUrl;
        IDictionary<string, string> Tabs;
        readonly Helpers.Sql sql = new Helpers.Sql($"DB/filters.sql", $"DB/bets.sql");
        bool pause = false;
        bool firstws = false;
        string mainWndwH = "";
        Helpers.Logger log;
        int opened = 0;


       

        public MainWindow()
        {
            DataContext = this;
            Filters = new ObservableCollection<FilterModel>();
            Bets = new ObservableCollection<BetModel>();
            IgnoreUrl = new Dictionary<string, DateTime>();
            Tabs = new Dictionary<string, string>();
            InitializeComponent();
            CurrentFilterInForm = new FilterModel();
            log = new Helpers.Logger();

        }

        readonly string betsUrl = "https://1xstavka.ru/office/history/";
        private async void InitSelenium()
        {
            var options = new ChromeOptions();
            options.BinaryLocation = $"GoogleChromePortable/GoogleChromePortable.exe";
            options.AddArgument("--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36");
            options.AddArgument("--disable-infobars");
            options.AddArgument("--disable-notifications");
            options.AddArgument("--disable-popup-blocking");
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-application-cache");
            options.AddArgument("--disable-logging");
            options.AddArgument("--ignore-certificate-errors");
            webDriver = new ChromeDriver(options);
            webDriver.Manage().Window.Size = new System.Drawing.Size(1920, 1080);
            webDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(20);


            string url = MainUrl + "live/Ice-Hockey/";
            for (int i = 0; true; i++)
            {
                if (!Tabs.ContainsKey(url))
                {

                    try
                    {
                        if (url != "https://1xstavka.ru/live/Ice-Hockey/" || !firstws)
                        {
                            webDriver.Navigate().GoToUrl(url);
                            Tabs.Add(url, webDriver.CurrentWindowHandle);
                        }

                        if (url == "https://1xstavka.ru/live/Ice-Hockey/" && !firstws)
                            mainWndwH = webDriver.CurrentWindowHandle;
                        if (url == "https://1xstavka.ru/live/Ice-Hockey/" && firstws)
                            webDriver.SwitchTo().Window(mainWndwH);
                    }
                    catch
                    {
                        try
                        {
                            log.Error("TIMEOUT", "OPEN URL TIME");
                            banUrl(url, DateTime.Now.AddDays(100));
                            if (webDriver.CurrentWindowHandle != mainWndwH)
                                webDriver.Close();
                            Tabs.Remove(url);
                        }
                        catch (Exception e)
                        {
                            if (e.Message.Contains("no such window") || e.Message.Contains("chrome not reachable") || e.Message.Contains("invalid session id"))
                            {
                                isWork = false;
                                KillChrome();
                                Tabs.Clear();
                                mainWndwH = "";
                                firstws = false;
                                log.Error(e.Message, "browser restarted because a problem");
                                InitButton_Click(null, null);
                                return;
                            }
                            continue;
                        }
                    }
                }
                else
                {
                    try
                    {

                        string TabHandle = Tabs[url];
                        webDriver.SwitchTo().Window(TabHandle);
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.Message, "open opened tab");
                        Tabs.Remove(url);
                        continue;
                    }
                }
                HtmlDocument html = new HtmlDocument();
                html.LoadHtml(webDriver.PageSource);
                try
                {
                    if (!firstws)
                    {
                        var login = html.DocumentNode.SelectSingleNode("//*[@id=\"loginout\"]/div/div/div/div[1]/span[1]");
                        webDriver.FindElement(By.XPath(login.XPath)).Click();

                        html.LoadHtml(webDriver.PageSource);
                        var loginButtonXpath = html.DocumentNode.SelectSingleNode("//*[@id=\"loginout\"]/div/div/div/div[2]/div/form/button").XPath;
                        Thread.Sleep(1000);
                        webDriver.FindElement(By.Id("auth_id_email")).SendKeys(LoginTB.Text);
                        Thread.Sleep(1000);
                        webDriver.FindElement(By.Id("auth-form-password")).SendKeys(PasswordTB.Text);
                        Thread.Sleep(1000);
                        webDriver.FindElement(By.XPath(loginButtonXpath)).Click();
                        Thread.Sleep(1000);
                        firstws = true;
                        bool ContinueCheck = true;
                        bool phone = false;
                        while (ContinueCheck)
                        {
                            try
                            {
                                html.LoadHtml(webDriver.PageSource);
                                HtmlNode money = html.DocumentNode.SelectSingleNode("/html/body/div[2]/div[1]/div[2]/span[1]/div/div/div/a");
                                HtmlNode phoneReque = html.DocumentNode.SelectSingleNode("//*[@id=\"phone_middle\"]");

                                if (phoneReque != null && phone != true)
                                {
                                    phone = true;
                                }

                                if (money != null)
                                {
                                    ContinueCheck = false;
                                    try
                                    {

                                        webDriver.Navigate().GoToUrl(url);
                                    }
                                    catch 
                                    {
                                        banUrl(url, DateTime.Now.AddDays(100)); 
                                        if (webDriver.CurrentWindowHandle != mainWndwH)
                                            webDriver.Close();
                                        Tabs.Remove(url);
                                        log.Error("timeout", "");
                                    }
                                    Tabs[url] = webDriver.CurrentWindowHandle;
                                }
                                await Task.Delay(1000);
                            }
                            catch
                            {

                            }
                        }
                    }
                }
                catch {}
                HtmlNodeCollection table = null;
                try
                {

                    table = html.DocumentNode.SelectNodes("//*[@id=\"games_content\"]/div/div[1]/div/div");
                }
                catch {
                    continue;
                }
                for (int k = 0; k < table.Count; k++)
                {
                    HtmlNode node = table[k];

                    
                    await Task.Delay(1);
                    var TournirNode = node.SelectSingleNode(node.XPath + "/div[1]/div[3]/a");

                    var nameTournir = TournirNode.InnerText;
                    var EventsNodes = node.SelectNodes(node.XPath + "/div");
                    await Task.Delay(1);

                    for (int j = 0; j < EventsNodes.Count; j++)
                    {

                        while (pause)
                            await Task.Delay(1000);
                        HtmlNode eventNode = EventsNodes[j];
                        if (eventNode == EventsNodes[0])
                            continue;
                        try
                        {
                            await Task.Delay(1);
                            string eventUrl = MainUrl + eventNode.SelectSingleNode(eventNode.XPath + "/div[1]/div[1]/div[1]/div[1]/a").Attributes["href"].Value;
                            CurentEventName = eventUrl;
                            await Task.Delay(1);

                            if (IgnoreUrl.ContainsKey(eventUrl) && IgnoreUrl[eventUrl] > DateTime.Now)
                                continue;
                            await Task.Delay(1);

                            string eventTime = eventNode.Descendants(0).Where(n => n.HasClass("c-events__time")).FirstOrDefault().InnerText;
                            await Task.Delay(1);

                            CurentLigue = nameTournir;
                            int minutes = -1;
                            int seconds = -1;
                            try
                            {

                                minutes = int.Parse(eventTime.Split(' ')[0].Split(':')[0]);
                                seconds = int.Parse(eventTime.Split(' ')[0].Split(':')[1]);
                            }
                            catch
                            {
                            }
                            Console.WriteLine("-------------------------------------------");
                            Console.WriteLine($"{nameTournir}, {eventUrl}, \n time {minutes}:{seconds}");
                            CurentEventMinuts = minutes;
                            CurentEventSeconds = seconds;
                            string state = eventTime.Substring(eventTime.IndexOf(' '), eventTime.Length - eventTime.IndexOf(' '));
                            await Task.Delay(1);

                            try
                            {
                                opened += 1;
                                await getCfs(eventUrl, webDriver);
                                if (opened != 0 && opened % 10 == 0)
                                {
                                    try
                                    {
                                        CloseUseles();
                                    }
                                    catch
                                    {
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                if (e.Message.Contains("timeout"))
                                {
                                    j++;
                                    if (webDriver.CurrentWindowHandle != mainWndwH)
                                        webDriver.Close();
                                    banUrl(eventUrl, DateTime.Now.AddDays(10));
                                    break;
                                }
                            }
                        }
                        catch
                        {
                            banUrl(url, DateTime.Now.AddDays(10));
                        }
                    }
                    
                }
                try
                {
                    CloseUseles();
                }
                catch
                {
                }
            }
            
            
            
        }

        private void GetBets()
        {
            HtmlDocument html = new HtmlDocument();
            IJavaScriptExecutor jse = (IJavaScriptExecutor)webDriver;
            jse.ExecuteScript($"window.open('','_blank');");
            webDriver.SwitchTo().Window(webDriver.WindowHandles.Last());
            webDriver.Navigate().GoToUrl(betsUrl);
            Thread.Sleep(2500);
            webDriver.FindElement(By.ClassName("show_history")).Click();
            Thread.Sleep(2500);
            html.LoadHtml(webDriver.PageSource);
            var sections = html.DocumentNode.SelectNodes("//*[@id=\"history_panel\"]/section");
            var BetsOnBk = new List<BetModel>();
            foreach (var section in sections)
            {
                try
                {

                    if (section.Attributes["class"].Value != "apm-panel open")
                        webDriver.FindElement(By.XPath(section.XPath + "/div[1]/div[1]/button")).Click();
                    var ligue = html.DocumentNode.SelectSingleNode(section.XPath + "/div[1]/div[1]/div[2]/p[1]").InnerText.Replace("2-х минутные удаления: ", "");
                    var time = html.DocumentNode.SelectSingleNode(section.XPath + "/div[1]/div[1]/div[1]/p[2]").InnerText.Split('|')[0].Replace("от", "").Replace(" ", "");
                    var teamsName = html.DocumentNode.SelectSingleNode(section.XPath + "/div[1]/div[1]/div[2]/p[2]").InnerText.Replace("2-х минутные удаления: ","");
                    var cf = html.DocumentNode.SelectSingleNode(section.XPath + "/div[1]/div[1]/div[7]").InnerText;
                    var line = html.DocumentNode.SelectSingleNode(section.XPath + "/div[2]/div/div[1]/div/div[4]/p[2]")
                        .InnerText
                        .Replace("Тотал (", "")
                        .Replace(")", "")
                        .Replace(" ", "");
                    DateTime tempTime3;
                    tempTime3 = DateTime.ParseExact(time, "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    BetModel bet = new BetModel
                        {   
                            BetDate = tempTime3.ToString()
                            , Cf = cf
                            , CommandsName = teamsName
                            , Event = "Не известно"
                            , Filter = "Не известно"
                            , IsReadOnly = true
                            , Ligue = ligue
                            , Line = line 
                        };
                    BetsOnBk.Add(bet);
                }
                catch (Exception E)
                {
                    log.Error("cant read section", "getbets");
                }

            }
            bool IsHave;
            int IsHaveJ;
            List<BetModel> BetToRemove = new List<BetModel>();
            DateTime tempTime = new DateTime(); 
            DateTime tempTime2;
            for (int i = 0; i < Bets.Count; i++)
            {
                IsHave = false;
                IsHaveJ = -1;

                tempTime = DateTime.ParseExact(Bets[i].BetDate.Split(' ')[0], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                for (int j = 0; j < BetsOnBk.Count; j++)
                {
                    tempTime2 = DateTime.ParseExact(BetsOnBk[j].BetDate.Split(' ')[0], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (Bets[i].CommandsName.Contains(BetsOnBk[j].CommandsName) && tempTime == tempTime2 && BetsOnBk[j].Line == Bets[i].Line)
                    {
                        IsHave = true;
                        IsHaveJ = j;
                        break;
                    }
                }
                if (IsHave && IsHaveJ != -1)
                {
                    Bets[i].Cf = BetsOnBk[IsHaveJ].Cf;
                    sql.UpdateBet(Bets[i]);
                }
                if (!IsHave)
                {
                    sql.DeleteBet(Bets[i]);
                    BetToRemove.Add(Bets[i]);
                }
            }
            foreach (var item in BetToRemove)
            {
                Bets.Remove(item);
            }
            for (int i = 0; i < BetsOnBk.Count; i++)
            {
                IsHave = false;
                IsHaveJ = -1;
                tempTime2 = DateTime.ParseExact(BetsOnBk[i].BetDate.Split(' ')[0], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                for (int j = 0; j < Bets.Count; j++)
                {
                    tempTime = DateTime.ParseExact(Bets[j].BetDate.Split(' ')[0], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (Bets[j].CommandsName.Contains(BetsOnBk[i].CommandsName) && tempTime == tempTime2 && BetsOnBk[i].Line == Bets[j].Line)
                    {
                        IsHave = true;
                        IsHaveJ = j;
                        break;
                    }
                }
                if (IsHave && IsHaveJ != -1)
                {
                    Bets[IsHaveJ].Cf = BetsOnBk[i].Cf;
                    sql.UpdateBet(Bets[IsHaveJ]);
                }
                if (!IsHave)
                {
                    sql.AddBet(BetsOnBk[i]);
                    Bets.Add(BetsOnBk[i]);
                }
            }
            webDriver.Close();
        }

        private void CloseUseles()
        {
            try
            {

                webDriver.SwitchTo().Window(mainWndwH);
            }
            catch 
            {
            }
            foreach (var item in IgnoreUrl.Keys)
            {
                if(!Tabs.ContainsKey(item))
                {
                    continue;
                }
                if (IgnoreUrl.ContainsKey(item) && IgnoreUrl[item] > DateTime.Now)
                {
                    try
                    {

                        webDriver.SwitchTo().Window(Tabs[item]);
                        if (webDriver.CurrentWindowHandle != mainWndwH)
                            webDriver.Close();
                        Tabs.Remove(item);
                    }
                    catch (Exception)
                    {
                        try
                        {
                            webDriver.SwitchTo().Window(Tabs[item]);
                            if (webDriver.CurrentWindowHandle != mainWndwH)
                                webDriver.Close();
                            Tabs.Remove(item);
                        }
                        catch (Exception ex)
                        {
                            log.Error(ex.Message, "Close window"); 
                            Tabs.Remove(item);
                        }
                    }
                }
            }
        }

        string[] lines = null;
        string[] cells = null;
        private void getCommandsNames(string title)
        {
            int indexOfDate=0;
            for (int i = 0; i < title.Length; i++)
            {
                if(char.IsDigit(title[i]))
                {
                    indexOfDate = i;
                    break;
                }
            }
            if(indexOfDate != 0)
                CurrentEventCommandsNames = title.Substring(0, indexOfDate);
        }
        private async Task getCfs(string url, IWebDriver driver)
        {
            if(lines == null)
            {
                lines = new string[3] { "1-й Период 2-х минутные удаления ", "2-й Период 2-х минутные удаления ", "2-х минутные удаления " };
            }
            if (cells == null)
                cells = new string[3] { "Тотал. 1-й Период 2-х минутные удаления", "Тотал. 2-й Период 2-х минутные удаления", "Тотал. 2-х минутные удаления" };
            bool HaveOut = false;
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
           if (!Tabs.ContainsKey(url))
            {

                jse.ExecuteScript($"window.open('','_blank');");
                await Task.Delay(100);
                Tabs.Add(url,driver.WindowHandles.Last());
                driver.SwitchTo().Window(Tabs[url]);
                await Task.Delay(1);
                try
                {
                driver.Navigate().GoToUrl(url);
                }
                catch (Exception e)
                {
                    throw e;
                }
                await Task.Delay(1);

            }
            else
            {
                try
                {

                    var TabHandle = Tabs[url];
                    driver.SwitchTo().Window(TabHandle);
                }
                catch (Exception e)
                {
                    log.Error(e.Message, "open tab");
                    banUrl(url, DateTime.Now.AddMinutes(5));
                }
            }

            try
            {
                getCommandsNames(webDriver.Title);
            }
            catch
            {
                log.Error("cant get commands name", "get_COMANDSNAME");
            }
            log.Info(CurrentEventCommandsNames, "getCommandNames");

            HtmlDocument html = new HtmlDocument();
            bool somthingClicked = false;
            IEnumerable<HtmlNode> nodes = null;
            html.LoadHtml(driver.PageSource);

            DateTime start_find = DateTime.Now;
            bool isLoad = false;
            var isLoadNode = html.DocumentNode.SelectSingleNode("//*[@id=\"user_messages_container\"]/div/a/svg");
            if (isLoadNode != null)
            {
                for (int k = 0; k < isLoadNode.Attributes.Count; k++)
                {
                    if (isLoadNode.Attributes[k].Name == "class" && isLoadNode.Attributes[k].Value.Contains("c-messages__svg--loading"))
                        isLoad = true;
                }
                while (isLoad)
                {
                    isLoad = false;
                    isLoadNode = html.DocumentNode.SelectSingleNode("//*[@id=\"user_messages_container\"]/div/a/svg");

                    for (int k = 0; k < isLoadNode.Attributes.Count; k++)
                    {
                        if (isLoadNode.Attributes[k].Name == "class" && isLoadNode.Attributes[k].Value.Contains("c-messages__svg--loading"))
                            isLoad = true;
                    }
                    if (start_find.AddSeconds(15) > DateTime.Now && isLoad)
                    {
                        banUrl(url, DateTime.Now.AddDays(100));
                        log.Error("timeout", "");
                        return;
                    }
                    if (!isLoad)
                        break;
                }
            }
            try
            {

                nodes =
                    html.DocumentNode.Descendants(0)
                        .Where(n => n.HasClass("scoreboard-nav__select"));
                driver.FindElement(By.XPath(nodes.FirstOrDefault().XPath)).Click();
            }
            catch 
            {
                try
                {
                    await Task.Delay(1000);
                    nodes =
                    html.DocumentNode.Descendants(0)
                        .Where(n => n.HasClass("scoreboard-nav__select"));
                    driver.FindElement(By.XPath(nodes.FirstOrDefault().XPath)).Click();
                }
                catch (Exception e)
                {

                    log.Error(e.Message, "click expand btn");
                    banUrl(url, DateTime.Now.AddMinutes(5));
                    return;
                }
            }
            nodes =
                    html.DocumentNode.Descendants(0)
                        .Where(n => n.HasClass("multiselect__element"));
            foreach (var item in nodes)
            {
                if(item.InnerText.Contains("Основная игра"))
                {
                    var scrollBar = driver.FindElements(By.ClassName("s-scoreboard-nav-multiselect")).FirstOrDefault();
                    string c = scrollBar.GetAttribute("class");
                    if (!c.Contains("multiselect--active"))
                        driver.FindElements(By.ClassName("multiselect__select")).FirstOrDefault().Click();
                    driver.FindElement(By.XPath(item.XPath + "/span")).Click();
                    await Task.Delay(200);
                    html.LoadHtml(driver.PageSource);

                    var scoreNodes = html.DocumentNode.Descendants(0)
                        .Where(n => n.HasClass("c-tablo-count__num"))
                        .ToArray();
                    int leftPos = int.Parse(scoreNodes[0].InnerText); //это и под удаления

                    int rightPos = int.Parse(scoreNodes[1].InnerText);
                    CurentEventScoreDiff = (int)Math.Sqrt((int)Math.Pow(leftPos - rightPos, 2));
                    log.Info($"score is {leftPos}:{rightPos}", "find match score");
                }
            }

            for (int i = 0; i < lines.Length; i++)
            {
                log.Info($"try to find {lines[i]}","getcf in for cycle");
                var click = driver.FindElements(By.ClassName("multiselect__element"));
                nodes =
                    html.DocumentNode.Descendants(0)
                        .Where(n => n.HasClass("multiselect__element"));
                bool clicked = false;
                
                foreach (var item in nodes)
                {
                    
                    if (item.InnerText == lines[i])
                    {
                        log.Info(item.InnerText, "element in grid");
                        try
                        {

                            var scrollBar = driver.FindElements(By.ClassName("s-scoreboard-nav-multiselect")).FirstOrDefault();
                            string c = scrollBar.GetAttribute("class");
                            if (!c.Contains("multiselect--active"))
                                driver.FindElements(By.ClassName("multiselect__select")).FirstOrDefault().Click();
                            driver.FindElement(By.XPath(item.XPath + "/span")).Click(); 
                            clicked = true;
                            somthingClicked = true;
                            log.Info("find out", "clicked element");
                        }
                        catch
                        {
                            try
                            {

                                var scrollBar = driver.FindElements(By.ClassName("s-scoreboard-nav-multiselect")).FirstOrDefault();
                                string c = scrollBar.GetAttribute("class");
                                if (!c.Contains("multiselect--active"))
                                    driver.FindElements(By.ClassName("multiselect__select")).FirstOrDefault().Click();

                                driver.FindElement(By.XPath(item.XPath + "/span")).Click();
                                clicked = true;
                                somthingClicked = true;
                                log.Info("find out", "clicked element");
                            }
                            catch (Exception)
                            {
                                try
                                {

                                    var scrollBar = driver.FindElements(By.ClassName("s-scoreboard-nav-multiselect")).FirstOrDefault();
                                    string c = scrollBar.GetAttribute("class");
                                    
                                    if (!c.Contains("multiselect--active"))
                                        driver.FindElements(By.ClassName("multiselect__select")).FirstOrDefault().Click();

                                    var elem = driver.FindElement(By.XPath(item.XPath + "/span"));
                                    Actions builder = new Actions(driver);

                                    for (int l = 0; l < 7; l++)
                                        builder.SendKeys(Keys.ArrowDown).Perform();
                                    nodes =
                                        html.DocumentNode.Descendants(0)
                                            .Where(n => n.HasClass("multiselect__element"));
                                    foreach(var it in nodes)
                                        if(it.InnerText == lines[i])
                                            elem = driver.FindElement(By.XPath(it.XPath + "/span"));
                                    elem.Click();
                                    somthingClicked = clicked = true;
                                    log.Info("find out", "clicked element");
                                }
                                catch (Exception)
                                {

                                }

                            }
                        }//ignore
                        
                    }
                }
                if ((i == (lines.Length - 1)) && !somthingClicked)
                    banUrl(url,DateTime.Now.AddMinutes(5));
                if (!clicked)
                    continue;
                await Task.Delay(500);
                html.LoadHtml(driver.PageSource);
                try
                {
                    getDeletes(html, i);
                }
                catch (Exception)
                {
                    continue;
                }

                html.LoadHtml(webDriver.PageSource);
                var betsCols = html.DocumentNode.SelectNodes("//*[@id=\"allBetsTable\"]/div");

                try
                {
                    foreach (var betsCol in betsCols)
                    {
                        var betsDivs = betsCol.SelectNodes(betsCol.XPath + "/div");
                        if (betsDivs != null)
                        {
                            foreach (var betsDiv in betsDivs)
                            {
                                var betsName = betsDiv.SelectSingleNode(betsDiv.XPath + "/div/div[1]").InnerText.Replace("\r\n", "");
                                if (betsName.Contains(cells[i]))
                                {

                                    var coefs = betsDiv.SelectNodes(betsDiv.XPath + "/div/div[2]/div");

                                    HaveOut = true;
                                    if (coefs == null)
                                    {
                                        try
                                        {

                                            var CoefNode = betsDiv.SelectSingleNode(betsDiv.XPath + "/div/div[2]/div");
                                            foreach (var atrib in CoefNode.Attributes)
                                            {
                                                if (atrib.Name == "class" && atrib.Value == "blockSob")
                                                {
                                                    log.Warn("cfs is lock", "getCfs");
                                                    return;
                                                }
                                                CurrentLineXpath = CoefNode.XPath;
                                                var nameOfBet = CoefNode.SelectSingleNode(CoefNode.XPath + "/span[1]").InnerText.Replace("\r\n", "").Replace(" ", "");
                                                var Betcoef = CoefNode.SelectSingleNode(CoefNode.XPath + "/span[2]").GetAttributeValue("data-coef", null);
                                                CurentEventCf = float.Parse(Betcoef.Replace('.', ','));
                                                CurentEventLine = nameOfBet;
                                                Console.WriteLine(nameOfBet + ":" + Betcoef);
                                                placeBet(url, i);
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            log.Warn("cfs is lock", "getCfs");
                                        }

                                    }
                                    else
                                    {
                                        foreach (var coef in coefs)
                                        {
                                            foreach (var atrib in coef.Attributes)
                                            {
                                                if(atrib.Name == "class" && atrib.Value== "blockSob")
                                                {
                                                    log.Warn("cfs is lock", "getCfs");
                                                    return;
                                                }

                                            }
                                            CurrentLineXpath = coef.XPath;
                                            var nameOfBet = coef.SelectSingleNode(coef.XPath + "/span[1]").InnerText.Replace("\r\n", "").Replace(" ", "");
                                            var Betcoef = coef.SelectSingleNode(coef.XPath + "/span[2]").GetAttributeValue("data-coef", null);
                                            CurentEventCf = float.Parse(Betcoef.Replace('.', ','));
                                            CurentEventLine = nameOfBet;
                                            Console.WriteLine(nameOfBet + ":" + Betcoef);
                                            placeBet(url, i);
                                        }
                                    }
                                    Console.WriteLine("\n ------------------------------------------");
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error(ex.ToString(), "parse cfs");
                    if (!HaveOut)
                    {
                        banUrl(url,DateTime.Now.AddMinutes(5));
                    }
                    return;
                }

                if (!HaveOut)
                {
                    banUrl(url, DateTime.Now.AddMinutes(5));
                }
            }
           
        }

        private void getDeletes(HtmlDocument html, int i)
        {
            List<HtmlNode> scoreNodes = new List<HtmlNode>();
            int leftDeletes;
            int rightDeletes;
            switch (i)
            {
                case 2:
                    scoreNodes = html.DocumentNode.Descendants(0)
                            .Where(n => n.HasClass("c-tablo-count__num"))
                            .ToList();
                    leftDeletes = int.Parse(scoreNodes[0].InnerText);

                    rightDeletes = int.Parse(scoreNodes[1].InnerText);
                    CurentEventDeletes = rightDeletes + leftDeletes;

                    log.Info(CurentEventDeletes.ToString(), "getDeletes");
                    return;
                case 1:
                    scoreNodes = html.DocumentNode.Descendants(0)
                           .Where(n => n.HasClass("c-time-period"))
                           .ToList();
                    if (scoreNodes.Count >= 2)
                    {
                        var left = html.DocumentNode.SelectSingleNode(scoreNodes[1].XPath + "/div[2]/div[1]");
                        var right = html.DocumentNode.SelectSingleNode(scoreNodes[1].XPath + "/div[2]/div[2]");
                        leftDeletes = int.Parse(left.InnerText.Replace(" ","").Replace("\n", "").Replace("\r", ""));
                        rightDeletes = int.Parse(right.InnerText.Replace(" ", "").Replace("\n", "").Replace("\r", ""));
                        CurentEventDeletes = rightDeletes + leftDeletes;

                        log.Info(CurentEventDeletes.ToString(), "getDeletes for 2nd time");
                    }
                    else throw new Exception();
                    return;
                case 0:
                    scoreNodes = html.DocumentNode.Descendants(0)
                           .Where(n => n.HasClass("c-time-period"))
                           .ToList();
                    if (scoreNodes.Count >= 1)
                    {
                        leftDeletes = int.Parse(html.DocumentNode.SelectSingleNode(scoreNodes[2].XPath + "/div[2]/div[1]").InnerText.Replace(" ", "").Replace("\n", "").Replace("\r", ""));
                        rightDeletes = int.Parse(html.DocumentNode.SelectSingleNode(scoreNodes[2].XPath + "/div[2]/div[2]").InnerText.Replace(" ", "").Replace("\n", "").Replace("\r", ""));
                        CurentEventDeletes = rightDeletes + leftDeletes;

                        log.Info(CurentEventDeletes.ToString(), "getDeletes for 1st time");
                    }
                    else throw new Exception();
                    return;
                default:
                    return;
            }
        }

        bool IsWriteFilters = false;
        private void banUrl(string url, DateTime banTime)
        {
            IgnoreUrl.Remove(url);
            IgnoreUrl.Add(url, banTime);
        }
        private void placeBet(string url, int iteration)
        {
            if (Filters.Count<1)
            {
                log.Info("upload filters", "placeBet");
                Filters = new ObservableCollection<FilterModel>();
                var tempFilters = sql.GetFilters();
                foreach (var item in tempFilters)
                {
                    Filters.Add(item);
                }
            }
            var bets = sql.GetBets();
            
            bool lgo;
                log.Info("start find needed filter", "placeBet");
            foreach (var item in Filters)
            {
                lgo = true;
                
                #region checkEnable
                if (item.Enable == false)
                    continue;
                #endregion
                string fixedLine = "";
                switch (iteration)
                {
                    case 0:
                        if (!string.IsNullOrEmpty(item.Line))
                            fixedLine = FixBet(item.Line1);
                        else log.Warn("skiped without line", "FixBet");
                        break;
                    case 1:
                        if (!string.IsNullOrEmpty(item.Line))
                            fixedLine = FixBet(item.Line2);
                        else log.Warn("skiped without line", "FixBet");
                        break;
                    case 2:
                        if (!string.IsNullOrEmpty(item.Line))
                            fixedLine = FixBet(item.Line);
                        else log.Warn("skiped without line", "FixBet");
                        break;
                }

                foreach (var bet in bets)
                {
                    if (bet.Event == CurentEventName && bet.Line == fixedLine)
                    {
                        log.Info($"{url} is sciped with {fixedLine}", "place bet");
                        return;
                    }
                }
                if (!lgo)
                {
                    log.Warn($"not passed with {CurentEventMinuts}:{CurentEventSeconds} and filter {item.Time}", "time check");
                }
                #region checkLigue
                if (lgo)
                {
                    if (item.Ligue.Contains(","))
                    {
                        foreach (var fltr in item.Ligue.Replace(" ","").Split(','))
                        {
                            if (fltr.Contains("!"))
                            {
                                if (CurentLigue.Contains(fltr.Replace("!", "")))
                                    lgo = false;
                            }
                            else
                            {
                                if (CurentLigue.Contains("*"))
                                    lgo = true;
                                else if (!CurentLigue.Contains(fltr))
                                    lgo = false;
                            }
                        }
                    }
                    else
                    {
                        if (item.Ligue.Contains("!"))
                        {
                            if (CurentLigue.Contains(item.Ligue.Replace("!", "")))
                                lgo = false;
                        }
                        else
                        {
                            if (item.Ligue.Contains("*"))
                                lgo = true;
                            else if (!CurentLigue.Contains(item.Ligue))
                                lgo = false;
                        }
                    }
                }

                if (!lgo) log.Warn("not passed", "check ligue");
                Thread.Sleep(100);

                #endregion
                #region checkLine
                if (lgo)
                {
                    if (fixedLine != CurentEventLine)
                        lgo = false;
                }
                if (!lgo)
                {
                    log.Warn($"{lines[iteration]} not passed with {CurentEventLine}", "Check line");
                    continue;
                }

                #endregion
                #region checkTime
                try
                {

                    int secMax;
                    int secMin;
                    int minMax;
                    int minMin;
                    secMax = int.Parse(item.Time.Split('-')[1].Split(':')[1]);
                    secMin = int.Parse(item.Time.Split('-')[0].Split(':')[1]);
                    minMax = int.Parse(item.Time.Split('-')[1].Split(':')[0]);
                    minMin = int.Parse(item.Time.Split('-')[0].Split(':')[0]);
                    if (secMax == 0)
                        secMax = 1;
                    if (secMin == 0)
                        secMin = 1;
                    if (minMax == 0)
                        minMax = 1;
                    if (minMin == 0)
                        minMin = 1;
                    if(minMax == 60)
                    {
                        minMax = 59;
                        secMax = 59;
                    }
                    var nowtime = new DateTime(2000, 1, 1, 1, CurentEventMinuts, CurentEventSeconds, 1);
                    var fltrTimeMin = new DateTime(2000, 1, 1, 1, minMin, secMin, 1);
                    var fltrTimeMax = new DateTime(2000, 1, 1, 1, minMax, secMax, 1);
                    if (!(fltrTimeMin < nowtime && nowtime < fltrTimeMax))
                        lgo = false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                #endregion
                #region checkDiffScore
                if (lgo)
                {
                    if (item.ScoreDiff.Contains(">=") && (int.Parse(item.ScoreDiff.Replace(">=", "")) >= CurentEventScoreDiff))
                        lgo = false;

                    else if (item.ScoreDiff.Contains("<=") && (int.Parse(item.ScoreDiff.Replace("<=", "")) <= CurentEventScoreDiff))
                        lgo = false;

                    else if (item.ScoreDiff.Contains(">") && (int.Parse(item.ScoreDiff.Replace(">", "")) > CurentEventScoreDiff))
                        lgo = false;

                    else if (item.ScoreDiff.Contains("<") && (int.Parse(item.ScoreDiff.Replace("<", "")) < CurentEventScoreDiff))
                        lgo = false;

                    else if (item.ScoreDiff.Contains("=") && (int.Parse(item.ScoreDiff.Replace("=", "")) != CurentEventScoreDiff))
                        lgo = false;
                    if (item.ScoreDiff.Contains("*"))
                        lgo = true;
                }
                if(!lgo) {
                    log.Warn("not passed", "check diff score ");
                    continue;

                }
                Thread.Sleep(100);

                #endregion
                #region checkDeletes
                if (lgo)
                {
                    try
                    {

                        if (item.Penalty.Contains("-"))
                        {
                            var lowerCase = int.Parse(item.Penalty.Replace(" ", "").Split('-')[0]);
                            var upperCase = int.Parse(item.Penalty.Replace(" ", "").Split('-')[1]);
                            if (CurentEventDeletes < lowerCase || CurentEventDeletes > upperCase)
                                lgo = false;

                        }
                        else if (item.Penalty.Contains("*"))
                            lgo = true;
                        else if (!item.Penalty.Contains("-"))
                        {
                            if (int.Parse(item.Penalty) != CurentEventDeletes)
                                lgo = false;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
                if (!lgo)
                {
                    log.Warn("not passed", "check deletes ");
                    continue;
                }
                Thread.Sleep(100);

                #endregion
                #region checkCF
                if (lgo)
                {
                    try
                    {

                        var lowerCase = float.Parse(item.Coef.Split('-')[0].Replace('.', ','));
                        var upperCase = float.Parse(item.Coef.Split('-')[1].Replace('.', ','));
                        if (CurentEventCf < lowerCase || CurentEventCf > upperCase)
                            lgo = false;
                    }
                    catch (Exception)
                    {
                    }
                }
                if (!lgo) log.Warn($"not passed by cf with filterid {item.id}","check cf");
                

                #endregion

                if (lgo)
                {
                    try
                    {

                        ReadOnlyCollection<IWebElement> coupons = webDriver.FindElements(By.ClassName("o-bet-box-list__item"));

                        foreach (var coupon in coupons)
                        {

                            try
                            {
                                coupon.FindElement(By.TagName("button")).Click();
                                coupons = webDriver.FindElements(By.ClassName("o-bet-box-list__item"));
                                if (coupons.Count == 0)
                                    break;
                            }
                            catch
                            {
                            }
                        } 
                        var coefbtn = webDriver.FindElement(By.XPath(CurrentLineXpath));
                        if(coefbtn.GetAttribute("class").Contains("blockSob"))
                        {
                            log.Warn("coef is locked", "bet pos");
                            return;
                        }
                        else
                        {
                            coefbtn.Click();
                        }
                        Thread.Sleep(500);
                        HtmlDocument html = new HtmlDocument();
                        html.LoadHtml(webDriver.PageSource);
                        var expandBtn = html.DocumentNode.SelectSingleNode("//*[@id=\"sports_right\"]/div/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div[2]/div[2]/div[2]/div/div");
                        bool isExpand = false;
                        if (!IsWriteFilters)
                        {
                            foreach (var fltr in Filters)
                            {
                                log.Info($"CF:{fltr.Coef} | ligue:{fltr.Ligue} | Line:{fltr.Line} | Line1:{fltr.Line1} | Line2:{fltr.Line2} | Penalty:{fltr.Penalty} | scoreDiff:{fltr.ScoreDiff} | time:{fltr.Time}", "");
                            }
                            IsWriteFilters = true;
                        }
                        foreach (var attr in expandBtn.Attributes)
                        {
                            if (attr.Value.Contains("multiselect--active") && attr.Name == "class")
                                isExpand = true;
                        }
                        if (!isExpand)
                            webDriver.FindElement(By.XPath(expandBtn.XPath)).Click();
                        var varriance = html.DocumentNode.SelectNodes("//*[@id=\"sports_right\"]/div/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[3]/ul/li");
                        foreach (var Var in varriance)
                        {
                            string text = html.DocumentNode.SelectSingleNode(Var.XPath + "/span/span").InnerText;
                            if(text.Contains("Принять при повышении"))
                            {
                                webDriver.FindElement(By.XPath(Var.XPath)).Click();
                                break;
                            }
                        }
                        var maxBetStr = html.DocumentNode.SelectSingleNode("//*[@id=\"sports_right\"]/div[1]/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div[2]/div[1]/div[1]/div/button").InnerText.Replace("\r", "").Replace("\n", "").Replace(" ", "");
                        var balanceStr = html.DocumentNode.SelectSingleNode("//*[@id=\"sports_right\"]/div[1]/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div[2]/div[1]/div[2]/div/button").InnerText.Replace("\r", "").Replace("\n", "").Replace(" ", "");
                        var Betcoef = html.DocumentNode.SelectSingleNode(CurrentLineXpath + "/span[2]").GetAttributeValue("data-coef", null); 
                        CurentEventCf = float.Parse(Betcoef.Replace('.', ','));

                        #region checkCF
                        if (lgo)
                        {
                            try
                            {

                                var lowerCase = float.Parse(item.Coef.Split('-')[0].Replace('.', ','));
                                var upperCase = float.Parse(item.Coef.Split('-')[1].Replace('.', ','));
                                if (CurentEventCf < lowerCase || CurentEventCf > upperCase)
                                    lgo = false;
                            }
                            catch (Exception)
                            {
                            }
                        }
                        if (!lgo) log.Warn($"not passed by cf with filterid {item.id}", "check cf");


                        #endregion
                        int maxBet = int.Parse(maxBetStr);
                        float curBalance = float.Parse(balanceStr.Replace('.',','));
                        int balance = int.Parse(BalanceTB.Text);
                        int bet = 0;
                        #region betSizeCheck
                        if (item.Betsize.Contains("%"))
                        {
                            var tempBetsize = item.Betsize.Replace("%", "");
                            bet = (int)((double.Parse(tempBetsize) / 100.0) * bank);
                            if (maxBet < bet)
                                bet = maxBet;
                        }
                        else if (!item.Betsize.Contains("%"))
                        {
                            bet = int.Parse(item.Betsize);
                            if (maxBet < bet)
                                bet = maxBet;
                        }
                        if ((bool)FixBetEnable.IsChecked)
                        {
                            bet = int.Parse(FixBetsize.Text);
                            if (maxBet < bet)
                                bet = maxBet;
                        }
                        #endregion// bet
                        var betInput = html.DocumentNode.SelectSingleNode("//*[@id=\"sports_right\"]/div[1]/div[2]/div/div[2]/div[1]/div/div[3]/div[2]/div[1]/div/div[3]/div/input");
                        webDriver.FindElement(By.XPath(betInput.XPath)).SendKeys(bet.ToString());
                        var betBtn = html.DocumentNode.SelectSingleNode("//*[@id=\"sports_right\"]/div[1]/div[2]/div/div[2]/div[1]/div/div[3]/div[3]/div/div/div/button");
                        webDriver.FindElement(By.XPath(betBtn.XPath)).Click(); 
                        var now = DateTime.Now.ToString();
                        var betModel = new BetModel { BetDate = now, Cf = CurentEventCf.ToString(), CommandsName = CurrentEventCommandsNames, Filter = item.id.ToString(), IsReadOnly = true, Ligue = CurentLigue, Line = fixedLine, Event = CurentEventName };

                        sql.AddBet(betModel);
                        try
                        {
                            IWebElement errorBtn = null;
                            try
                            {
                                errorBtn = webDriver.FindElement(By.ClassName("swal2-confirm"));
                                log.Info(errorBtn == null ? "no error" : "error", "confirm bet");
                            }
                            catch (Exception)
                            {
                                log.Info("error", "confirm bet");
                            }
                            if (errorBtn == null)
                            {
                                
                                IgnoreUrl.Add(url, DateTime.Now.AddSeconds(30));
                                log.Info("place bet", "click accept button");
                                webDriver.FindElements(By.ClassName("c-btn--gradient-accent"))[0].Click();
                            }
                            else
                            {
                                if (webDriver.CurrentWindowHandle != mainWndwH)
                                    webDriver.Close();
                                sql.DeleteBet(betModel);
                                var errorText = html.DocumentNode.SelectSingleNode("//*[@id=\"swal2-content\"]");
                                if (errorText != null)
                                    log.Warn("place bet error" + errorText.InnerText, "place bet btn");
                            }
                        }
                        catch (Exception)
                        {
                            try
                            {
                                webDriver.FindElement(By.ClassName("o-btn-group__item")).Click();
                                if (webDriver.CurrentWindowHandle != mainWndwH)
                                    webDriver.Close();
                            }
                            catch
                            {
                                Console.WriteLine("Ошибка ставки");
                            }
                        }

                        GetBets();
                    }
                    catch (Exception ex)
                    {
                        log.Error(ex.ToString(), "place bet");
                    }
                }
            }
        }

        private string FixBet( string curBetLine)
        {
            try
            {

                bool IsM = curBetLine.Contains("М");
                float CurDel = (float)CurentEventDeletes;
                string curblstr = curBetLine.Replace("Б", "").Replace("М", "").Replace('.', ',');
                float curBL = float.Parse(curblstr);
                if (curBL > CurDel+0.5)
                    return curBetLine;
                string retStr = (CurDel + curBL).ToString().Replace(',', '.');
                if (IsM)
                    retStr += "М";
                else
                    retStr += "Б";
                return retStr;
            }
            catch (Exception ex)
            {
                log.Error(ex.ToString(), "fix bet line");
                return "";
            }

        }

        public void Exit()
        {
            Exception e = null;
            while (e == null)
            {
                try
                {
                    webDriver.Close();
                    Thread.Sleep(200);
                }
                catch (Exception ex)
                {
                    e = ex;
                }
            }
        }
        private void Bets_Button_Click(object sender, RoutedEventArgs e)
        {
            Bets.Clear();
            foreach (var item in sql.GetBets())
            {
                Bets.Add(item);
            }
            
        }

        private void Filters_Button_Click(object sender, RoutedEventArgs e)
        {
            Filters.Clear();
            bool flag;
            foreach (var item in sql.GetFilters())
            {
                flag = true;
                foreach (var itemm in Filters)
                {
                    if (itemm.id == item.id)
                    {
                        flag = false;
                        break;
                    }
                }
                if(flag)
                    Filters.Add(item);
            }
            flag = true;
            foreach (var item in Filters)
            {
                if (item.id == null)
                    flag = false;
            }
            if(flag)
                Filters.Add(new FilterModel { id = null, Enable=false, Betsize="", Coef = "", Ligue= "", Line = "", Line1 = "", Line2="", Penalty="", ScoreDiff = "", Time ="" });
        }

        private void Grid_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {

            var item = (Grid)sender;
            var filter = (FilterModel)item.DataContext;
            if (CurrentFilterInForm.id != filter.id)
                CurrentFilterInForm = filter;
            else if (e.Key == System.Windows.Input.Key.Enter)
            {
                FillCurrentFilter(filter);
                if (CurrentFilterInForm.id == null)
                    sql.AddFilter(CurrentFilterInForm);
                else
                    sql.UpdateFilter(CurrentFilterInForm);
                    Filters_Button_Click(null, null);
            }
        }

        private void FillCurrentFilter(FilterModel filter)
        {
            if (CurrentFilterInForm.id == null)
                CurrentFilterInForm.id = filter.id;
            if (string.IsNullOrEmpty(CurrentFilterInForm.Ligue))
                CurrentFilterInForm.Ligue = filter.Ligue;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Time))
                CurrentFilterInForm.Time = filter.Time;

            if (string.IsNullOrEmpty(CurrentFilterInForm.ScoreDiff))
                CurrentFilterInForm.ScoreDiff = filter.ScoreDiff;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Penalty))
                CurrentFilterInForm.Penalty = filter.Penalty;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Line))
                CurrentFilterInForm.Line = filter.Line;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Line1))
                CurrentFilterInForm.Line1 = filter.Line1;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Line2))
                CurrentFilterInForm.Line2 = filter.Line2;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Coef))
                CurrentFilterInForm.Coef = filter.Coef;

            if (string.IsNullOrEmpty(CurrentFilterInForm.Betsize))
                CurrentFilterInForm.Betsize = filter.Betsize;

        }

        private void TabControl_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string tabItem = ((sender as TabControl).SelectedItem as TabItem).Header as string;
            switch (tabItem)
            {
                case "Ставки":
                    Bets_Button_Click(null, null);
                    break;
                case "Фильтры":
                        Filters_Button_Click(null, null);
                    break;
                default:
                    break;
            }

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var item = (TextBox)sender;
            switch (item.Name)
            {
                case "Ligue":
                    CurrentFilterInForm.Ligue = item.Text;
                    break;
                case "Time":
                    CurrentFilterInForm.Time = item.Text;
                    break;
                case "ScoreDiff":
                    CurrentFilterInForm.ScoreDiff = item.Text;
                    break;
                case "Penalty":
                    CurrentFilterInForm.Penalty = item.Text;
                    break;
                case "Line":
                    CurrentFilterInForm.Line = item.Text;
                    break;
                case "Line1":
                    CurrentFilterInForm.Line1 = item.Text;
                    break;
                case "Line2":
                    CurrentFilterInForm.Line2 = item.Text;
                    break;
                case "Coef":
                    CurrentFilterInForm.Coef = item.Text;
                    break;
                case "Betsize":
                    CurrentFilterInForm.Betsize = item.Text;
                    break;
            }
        }

        private void Grid_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var item = (Grid)sender;
            var filter = (FilterModel)item.DataContext;
            CurrentFilterInForm = filter;
        }

        private void ChangeFilter(string line, int curDeletes)
        {

        }
        private void pauseButton_Click(object sender, RoutedEventArgs e)
        {
            pause = true;
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            pause = false;
        }
        bool isWork = false;
        private async void InitButton_Click(object sender, RoutedEventArgs e)
        {
            if (!isWork)
            {
                if (string.IsNullOrEmpty(LoginTB.Text) || string.IsNullOrEmpty(BalanceTB.Text) || string.IsNullOrEmpty(PasswordTB.Text))
                {
                    MessageBox.Show("Укажите не достающие элементы в настройках или загрузите конфигурационный файл");
                    return;
                }
                await Task.Delay(1);
                try
                {
                    InitSelenium();
                    isWork = true;
                }
                catch (Exception E)
                {
                    log.Error(E.Message, "browser restarted because a problem");
                    isWork = false;
                    KillChrome();
                    InitButton_Click(null, null);
                }
            }
        }

        private void KillChrome()
        {
            Exit();
            try
            {
                var p = Process.GetProcessesByName("chromedriver");
                var pr = Process.GetProcessesByName("chrome");
                foreach (var item in p)
                {
                    item.Kill();
                }
                foreach (var item in pr)
                {
                    item.Kill();
                }
            }
            catch 
            {

            }
        }

        private void LoadButton_Click(object sender, RoutedEventArgs e)
        {
            string encJson;
            try
            {

                encJson = File.ReadAllText("settings.cfg");
            }
            catch (Exception)
            {
                MessageBox.Show("CFG file not found");
                return;
            }
            try
            {
                string json = encJson; // Decrypt(encJson, key);
                Models.Settings settings = Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(json);
                LoginTB.Text = settings.Login;
                PasswordTB.Text = settings.Password;
                BalanceTB.Text = settings.Balance;
                FixBetsize.Text = settings.FixBetSize;
                FixBetEnable.IsChecked = settings.FixBetSizeEnable;
                bank = int.Parse(settings.Balance);
            }
            catch (Exception)
            {
                MessageBox.Show("Не правильный пароль");
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(LoginTB.Text) || string.IsNullOrEmpty(BalanceTB.Text) || string.IsNullOrEmpty(PasswordTB.Text))
            {
                MessageBox.Show("Укажите не достающие элементы в настройках или загрузите конфигурационный файл");
                return;
            }
            Settings st = new Settings();
            st.Login = LoginTB.Text;
            st.Password = PasswordTB.Text;
            st.Balance = BalanceTB.Text;
            st.FixBetSize = FixBetsize.Text;
            st.FixBetSizeEnable = (bool)FixBetEnable.IsChecked;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(st);
            File.WriteAllText("settings.cfg", json);
        }

        
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            var s = (CheckBox)sender;
            CurrentFilterInForm.Enable = s.IsEnabled;
            sql.UpdateFilter(CurrentFilterInForm);
        }

        private void Image_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            sql.DeleteFilter(CurrentFilterInForm);
        }

        private void Out_Click(object sender, RoutedEventArgs e)
        {
            Excel.Application exApp = new Excel.Application();

            exApp.Workbooks.Add();
            Excel.Worksheet wsh = (Excel.Worksheet)exApp.ActiveSheet;

            exApp.Visible = true;
            if (Bets == null)
            {
                var b = sql.GetBets();
                foreach (var item in b)
                {
                    Bets.Add(item);
                }
            }
            wsh.Cells[1, 1] = "Дата ставки";
            wsh.Cells[1, 2] = "Лига";
            wsh.Cells[1, 3] = "Событие";
            wsh.Cells[1, 4] = "Линия";
            wsh.Cells[1, 5] = "Кф";
            wsh.Cells[1, 6] = "Номер Фильтра";
            wsh.Cells[1, 7] = "Названия команд";
            for (int i = 0; i < Bets.Count(); i++)
            {
                int counter = 1;
                wsh.Cells[i + 2, counter] = Bets[i].BetDate;
                counter++;
                wsh.Cells[i + 2, counter] = Bets[i].Ligue;
                counter++;
                wsh.Cells[i + 2, counter] = Bets[i].Event;
                counter++;
                wsh.Cells[i + 2, counter] = Bets[i].Line;
                counter++;
                wsh.Cells[i + 2, counter] = Bets[i].Cf;
                counter++;
                wsh.Cells[i + 2, counter] = Bets[i].Filter;
                counter++;
                wsh.Cells[i + 2, counter] = Bets[i].CommandsName;

            }
            sql.DeleteAllBets();
            Bets.Clear();
        }
    }
}
