﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _1xBot.Models
{
    public class BetModel
    {
        [PrimaryKey,AutoIncrement]
        public int Id { get; set; }
        public string BetDate {get;set;}
        public string Ligue { get; set; }
        public string Event { get; set; }
        public string Line { get; set; }
        public string Cf { get; set; }
        public string Filter { get; set; }
        public bool IsReadOnly { get; set; }
        public string CommandsName { get; set; }
    }
}
